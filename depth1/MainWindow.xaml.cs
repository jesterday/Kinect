﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.IO;
using System.Net;
using System.Web;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading;
//using MySql.Data.MySqlClient;
//using MySql.Data;
//using MySql.Web;

namespace depth1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private KinectSensor _Kinect;
        private short[] _DepthPixelData;
        //private MySqlConnection conn = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=kinect");
        //private MySqlConnection conn = new MySqlConnection("datasource=127.0.0.1;port=3306;username=root;password=;database=kkinect");
        private byte[] _ColorPixelData;
        List<int> list1 = new List<int>();
        private int[] _depths2;
        private DateTime now;
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += (s, e) => { DiscoverKinectSensor(); };
            this.Unloaded += (s, e) => { this.Kinect = null; };
            //mulai.Click += mulai_click;
        }

        
        private void DiscoverKinectSensor()
        {
            KinectSensor.KinectSensors.StatusChanged += KinectSensors_StatusChanged;
            this.Kinect = KinectSensor.KinectSensors
            .FirstOrDefault(x => x.Status == KinectStatus.Connected);
        }
        private void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case KinectStatus.Connected:
                    if (this.Kinect == null)
                    {
                        this.Kinect = e.Sensor;
                    }
                    break;
                case KinectStatus.Disconnected:
                    if (this.Kinect == e.Sensor)
                    {
                        this.Kinect = null;
                        this.Kinect = KinectSensor.KinectSensors
                        .FirstOrDefault(x => x.Status == KinectStatus.Connected);
                        if (this.Kinect == null)
                        {
                        }
                    }
                    break;
            }
        }
        public KinectSensor Kinect
        {
            get { return this._Kinect; }
            set
            {
                if(this._Kinect != value)
                {
                    if(this._Kinect != null)
                    {
                        //Uninitialize
                        this._Kinect = null;
                    }
                    if(value != null && value.Status == KinectStatus.Connected)
                    {
                        this._Kinect = value;
                        InitializeKinectSensor(this._Kinect);
                    }
                }
            } 
        }
        private void InitializeKinectSensor(KinectSensor sensor)
        {
            if (sensor != null)
            {
                /*sensor.ColorStream.Enable(ColorImageFormat.InfraredResolution640x480Fps30);
                sensor.ColorFrameReady += Kinect_ColorFrameReady;*/
                //sensor.ColorFrameReady += Kinect_InfraredReady;
                sensor.DepthStream.Enable();
                sensor.DepthFrameReady += KinectDevice_DepthFrameReady;
                sensor.Start();
            }
        }

        /*private void Kinect_InfraredReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame frame = e.OpenColorImageFrame())
            {
                if (frame != null)
                {
                    byte[] pixelData = new byte[frame.PixelDataLength];
                    frame.CopyPixelDataTo(pixelData);
                    ColorImage.Source = BitmapSource.Create(frame.Width, frame.Height, 96, 96,
                        PixelFormats.Gray16, null, pixelData,
                        frame.Width * 2);
                }
            }
        }*/

        private void TakePicture()
        {
            //conn.Open();
            //MySqlCommand cmd = conn.CreateCommand();
            //now = DateTime.Now;
            /*var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            DateTime dt = DateTime.Now; // Or whatever
            string tgl = dt.ToString("yyyy-MM-dd HH:mm:ss");
            //String tgl = now.ToString("dd-MM-yyyy HH:mm:ss");
            //sas1.Content = tgl;
            String name = @"D:\\tes\"+ tgl+ ".jpg";
            string fileName = name;

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }*/
            /*using (FileStream savedSnapshot = new FileStream(fileName, FileMode.CreateNew))
            {
                BitmapSource image = (BitmapSource)ColorImage.Source;
                JpegBitmapEncoder jpgEncoder = new JpegBitmapEncoder();
                jpgEncoder.QualityLevel = 70;
                jpgEncoder.Frames.Add(BitmapFrame.Create(image));
                jpgEncoder.Save(savedSnapshot);
                savedSnapshot.Flush();
                savedSnapshot.Close();
                savedSnapshot.Dispose();
                //sas.Content = "Sirene Bunyi";
               String urlAddress = "http://192.168.43.39:3000/run";

                using (WebClient client = new WebClient())
                {
                    NameValueCollection postData = new NameValueCollection() 
                   { 
                          { "data", "a" }
                   };

                    string pagesource = Encoding.UTF8.GetString(client.UploadValues(urlAddress, postData));
                }
                sas.Content = "Sirene Nyala";                
                
            }*/
            /*String urlAddress = "http://192.168.43.39:3000/run";

            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection() 
                   { 
                          { "data", "a" }
                   };

                string pagesource = Encoding.UTF8.GetString(client.UploadValues(urlAddress, postData));
            }*/
            sas.Content = "Sirene Nyala";
            //MessageBox.Show("Success");
            
            //Console.ReadLine();
            //conn.Close();
            //sas.Content = "Sirene Nyala";
        } 

        private void Kinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame frame = e.OpenColorImageFrame())
            {
                if (frame != null)
                {
                    byte[] pixelData = new byte[frame.PixelDataLength];
                    frame.CopyPixelDataTo(pixelData);

                    /*ColorImage.Source = BitmapImage.Create(frame.Width, frame.Height, 96, 96,
 PixelFormats.Bgr32, null, pixelData,
 frame.Width * frame.BytesPerPixel);*/ 
                    ColorImage.Source = BitmapSource.Create(frame.Width, frame.Height, 96, 96,
                        PixelFormats.Gray16, null, pixelData,
                        frame.Width * 2);
                }
            }
        }

        private void KinectDevice_DepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            using (DepthImageFrame frame = e.OpenDepthImageFrame())
            {
                if (frame != null)
                {
                    short[] pixelData = new short[frame.PixelDataLength];
                    frame.CopyPixelDataTo(pixelData);
                    int stride = frame.Width * frame.BytesPerPixel;
                    DepthImage.Source = BitmapSource.Create(frame.Width, frame.Height, 96, 96,
                    PixelFormats.Gray16, null, pixelData, stride);
					frame.CopyPixelDataTo(pixelData);
                    //CreateBetterShadesOfGray(frame, this._DepthPixelData); //See Listing 3-8
                    CreateDepthHistogram(frame, pixelData);
                    /*for (int i = 0; i < pixelData.Length; i++) {
                        int j = pixelData[i];
                        Console.WriteLine(j);
                        
                    }*/
                    //Console.WriteLine("STOP");
                    
                }
            } 
        }


        private void CreateDepthHistogram(DepthImageFrame depthFrame, short[] pixelData)
        {
            
            
            int depth;
            short[] depths1 = new short[pixelData.Length];
            List<int> list = new List<int>();
            List<int> list2 = new List<int>();
            int maxValue = 0;
            
            label.Content = "Value : ";
            label1.Content = "Status : ";
            label2.Content = "Max : ";
            label3.Content = "Min : ";
            double chartBarWidth = DepthHistogram.ActualWidth / depths1.Length;

            DepthHistogram.Children.Clear();
           
            //First pass - Count the depths.
            for (int i = 0; i < depthFrame.PixelDataLength; i += depthFrame.BytesPerPixel)
            {
                depth = (pixelData[i]) >> DepthImageFrame.PlayerIndexBitmaskWidth;
                if (depth > 0)
                {
                    depths1[depth]++;
                    list.Add(depth);
                }
            }
            //Second pass - Find the max depth count to scale the histogram to the space available.
            // This is only to make the UI look nice.
            int[] depths = list.ToArray();
            int sum = depths.Sum();
            list1.Add(sum);
            list2.Add(sum);
            int percent = 8;
            int param;

            this._depths2 = list1.ToArray();

            int[] depths3 = list2.ToArray();
            param = depths3[0];
            sas1.Content = param.ToString();


            
            int min, max;
            int persen = this._depths2[0] * percent / 100;
            min = this._depths2[0] - persen;
            max = this._depths2[0] + persen;

            if (param > max || param < min)
            {
                TakePicture();
            }
            else {
                sas.Content = "Sirene Mati";
            }
            
            




            sas2.Content = max.ToString();
            sas3.Content = min.ToString();
            
            
            for (int i = 0; i < depths1.Length; i++)
            {
                maxValue = Math.Max(maxValue, depths1[i]);
            }
            //Third pass - Build the histogram.
            
            
            
            
            for (int i = 0; i < depths1.Length; i++)
            {
                if (depths1[i] > 0)
                {
                    Rectangle r = new Rectangle();
                    r.Fill = Brushes.Black;
                    r.Width = 5;
                    r.Height = DepthHistogram.ActualHeight * (depths1[i] / (double)maxValue);
                    r.Margin = new Thickness(1, 0, 1, 0);
                    r.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                    DepthHistogram.Children.Add(r);
                }
            }
            
        } 

    }
}
